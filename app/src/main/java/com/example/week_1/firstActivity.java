package com.example.week_1;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

/**
 * This Activity have 4 buttons in it. Make it so each button when pressed play a sound for the user.
 * You'll find 4 sample sounds in the "raw" folder under "res".
 *
 * You will need to listen for button press and then probably use "SoundPool" or "MediaPlayer", both classes found inside
 * the Android framework. Take the opportunity to research which one is "right" to use when.
 */
public class firstActivity extends AppCompatActivity {

    int sound;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void onClickSound(View view) {
        if (view.getId() == R.id.imgBtn_1) {
            sound = 1;
            startSound(sound);
        }
        if (view.getId() == R.id.imgBtn_2) {
            sound = 2;
            startSound(sound);
        }
        if (view.getId() == R.id.imgBtn_3) {
            sound = 3;
            startSound(sound);
        }
        if (view.getId() == R.id.imgBtn_4) {
            sound = 4;
            startSound(sound);
        }
    }


    public void startSound(int sound) {
        releaseMediaPlayer();

        if (sound == 1) {
             mediaPlayer = MediaPlayer.create(this, R.raw.sound1);
            mediaPlayer.start();
        }
        if (sound == 2) {
             mediaPlayer = MediaPlayer.create(this, R.raw.sound2);
            mediaPlayer.start();
        }
        if (sound == 3) {
             mediaPlayer = MediaPlayer.create(this, R.raw.sound3);
            mediaPlayer.start();
        }
        if (sound == 4) {
             mediaPlayer = MediaPlayer.create(this, R.raw.sound4);
            mediaPlayer.start();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releaseMediaPlayer();
    }

    private void releaseMediaPlayer() {
        if (mediaPlayer != null) mediaPlayer.release();
    }
}
