package com.example.week_1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

/**
 * In this activity you should take a closer look at the life cycle of an activity.
 *
 * Read: https://developer.android.com/guide/components/activities/activity-lifecycle
 *
 * STEP 1:
 * And then add a override for the different "on" methods you can identify.
 * Don't forget the "onRestoreInstanceState" and "onSaveInstanceState" methods.
 *
 * Add a simple toast in the methods to track when they occur.
 *
 * (also more here: https://developer.android.com/topic/libraries/architecture/lifecycle )
 *
 * STEP 2:
 * We now also want to check how we can track if the orientation of the screen changes.
 * The resource for how this is made can be found here:
 * https://developer.android.com/guide/topics/resources/runtime-changes
 *
 * (Notice that you have to update the Manifest file, more info: https://developer.android.com/guide/topics/manifest/manifest-intro )
 */
public class thirdActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Toast.makeText(getApplicationContext(), "onCreate called", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(getApplicationContext(), "onPause called", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(getApplicationContext(), "onResume called", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(getApplicationContext(), "onStart called", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(getApplicationContext(), "onStop called", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(getApplicationContext(), "onRestart called", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(getApplicationContext(), "onDestroy called", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Toast.makeText(getApplicationContext(), "onRestoreInstanceState called", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Toast.makeText(getApplicationContext(), "onSaveInstanceState called", Toast.LENGTH_LONG).show();
    }

}
