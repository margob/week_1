package com.example.week_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * We have a lot of buttons in the view! A listener is added but we're missing the INTENTs
 * It's your job to fix this so we navigate to the correct activity!
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setListeners();
    }

    private void setListeners() {
        findViewById(R.id.btn_act_1).setOnClickListener(this);
        findViewById(R.id.btn_act_2).setOnClickListener(this);
        findViewById(R.id.btn_act_3).setOnClickListener(this);
        findViewById(R.id.btn_act_4).setOnClickListener(this);
        findViewById(R.id.btn_act_5).setOnClickListener(this);
        findViewById(R.id.btn_act_6).setOnClickListener(this);

        //TODO probably more.


    }

    @Override
    public void onClick(View v) {
        Button btn = (Button)v;

        switch (btn.getId()) {
            case R.id.btn_act_1 : {
Intent intentOne =new Intent(getApplicationContext(), firstActivity.class);
startActivity(intentOne);
                //Navigate to firstActivity

                break;
            } case R.id.btn_act_2 : {
                //Navigate to secondActivity

                break;
            } case R.id.btn_act_3 : {
                Intent intent = new Intent(getApplicationContext(), thirdActivity.class);
                startActivity(intent);

                break;
            } case R.id.btn_act_4 : {
                //Navigate to fourthActivity
                Intent intent = new Intent(getApplicationContext(), fourthActivity.class);
                startActivity(intent);

                break;
            } case R.id.btn_act_5 : {
                //Navigate to fifthActivity

                break;
            } case R.id.btn_act_6 : {
                //Navigate to sixthActivity
                
                break;
            }
            //TODO more to come i think.
        }
    }
}
