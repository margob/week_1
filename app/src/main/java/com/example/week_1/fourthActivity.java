package com.example.week_1;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

/**
 * In this activity the goal is to create a small "3 in a row" game. If you look in the corresponding layout file
 * You'll see 9 imageButtons. The goal here is that you can play this with a friend (using the same phone).
 *
 * So if user ONE presses a button the button image should change to "X" "cross.png in drawable".
 * Then it's user TWO's turn, when he presses the image button should instead turn "O" "circle.png in drawable".
 *
 * This should ofc alternate until all the buttons have been pressed.
 */
public class fourthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


}
