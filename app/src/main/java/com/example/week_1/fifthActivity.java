package com.example.week_1;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

/**
 * This activity might be a tough one!
 *
 * In the view you'll find a "OPEN CONTACTS" button.
 * When the button is pressed the "openContacts" method will fire.
 *
 * Your goal is to then open the contact list in your phone. In the list the user will select a contact,
 * When this is done the phone will hand back the control to this activity and the method "onActivityResult".
 *
 * You then need to retrieve the name of the contact selected and display that name in a toast for the user.
 *
 * resources:
 * Is this the hard part?
 */
public class fifthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void openContacts(View v) {

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);


    }

}
