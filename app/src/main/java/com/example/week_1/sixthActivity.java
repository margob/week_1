package com.example.week_1;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

/**
 * all eye's are on you!
 *
 * Given the small topics we have covered or something in the same vicinity for that matter.
 * Create a "challenge" that you then can pass along in the forum to your classmates.
 *
 * When you then share your challenge on the forums think about not posting the solution to rest of the challenges
 * (since you know, opportunity makes the lazy human).
 */
public class sixthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sixth);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

}
