package com.example.week_1;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

/**
 * This activity should have an listener for clicks on the "GO" button. When the "GO" button is clicked an INTENT should be
 * made to send the text inside the textView to the "receiverActivity". The "receiverActivity" should then display the text
 * inside it's textView.
 *
 * If you don't remember how to do this. Have a look at: https://developer.android.com/training/basics/firstapp/starting-activity again.
 */
public class secondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

}
